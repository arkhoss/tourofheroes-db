-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tourofheroes
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tourofheroes` ;

-- -----------------------------------------------------
-- Schema tourofheroes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tourofheroes` DEFAULT CHARACTER SET utf8 ;
USE `tourofheroes` ;

-- -----------------------------------------------------
-- Table `tourofheroes`.`hero`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tourofheroes`.`hero` ;

CREATE TABLE IF NOT EXISTS `tourofheroes`.`hero` (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
