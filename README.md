# Tourofheroes

This project was generated to work as DB repository for ThourOfHeroes APP

## How to run this repo
Single container
```
  docker run -d --restart=unless-stopped --log-driver json-file --net="host" -p 3306:3306 -v ./sql-scripts/tourofheroes.sql:/docker-entrypoint-initdb.d/1-tourofheroes.sql -v ./sql-scripts/tourofheroes-demo-data.sql:/docker-entrypoint-initdb.d/2-tourofheroes-demo-data.sql -e "MYSQL_ROOT_PASSWORD=1234" --name tourofheroes-db_db_1 arkhoss/tourofheroes-db 
```

Run
```
  docker-compose up -d
```

Stop and Delete
```
  docker-compose down
```

## How to login
```
docker exec -ti tourofheroes-db_db_1 mysql -u root -p
```

## License
MIT

## Author
David Caballero
